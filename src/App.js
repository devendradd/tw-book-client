import { Navbar, FormControl, Nav, NavDropdown, Form, Button } from 'react-bootstrap';
import React from 'react';
import logo from './logo.svg';
import './App.css';

import Books from './components/Books/Books.js';

function App() {
  return (
    <div className="App">
      <div>
      <Navbar bg="light" expand="lg">
        <Navbar.Brand href="#home">Books</Navbar.Brand>
        <Navbar.Toggle aria-controls="basic-navbar-nav" />
          <Navbar.Collapse id="basic-navbar-nav">
          <Nav className="mr-auto">
            <Nav.Link href="#home">Home</Nav.Link>
          </Nav>
          </Navbar.Collapse>
        </Navbar>
      </div>
      <div>
        <Books />
      </div>

    </div>
  );
}

export default App;
