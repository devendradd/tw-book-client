import React, { Component } from 'react';
import { Modal, Button } from 'react-bootstrap';
import axios from 'axios';

export default class EditBook extends React.Component{
  constructor(props){
    super(props);
      this.state={show:false}
      this.handleShow = this.handleShow.bind(this);
      this.handleClose = this.handleClose.bind(this);
  }

  componentWillMount(){
    console.log("in componentWillMount");
  }

  handleClose() {
    this.setState({ show: false });
  }

  handleShow() {
    console.log("here");
    this.setState({ show: true });
  }

  render(){
    console.log("in render");
    if(this.state.show == true){
        var displayString = (
          <Modal show={this.state.show} onHide={this.handleClose}>
            <Modal.Header closeButton>
              <Modal.Title>Edit Book</Modal.Title>
            </Modal.Header>
            <Modal.Body>
              <Container>
                <Row>
                  <Col>1 of 2</Col>
                  <Col>2 of 2</Col>
                </Row>
              </Container>
            </Modal.Body>
            <Modal.Footer>
              <Button variant="secondary" onClick={this.handleClose}>
                Close
              </Button>
              <Button variant="primary" onClick={this.handleClose}>
                Save Changes
              </Button>
            </Modal.Footer>
          </Modal>
        )
    }
    return(
      <div>
        {displayString}
      </div>
    )
  }
}
