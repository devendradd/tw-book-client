import React, { Component } from 'react';
import { Col, Row, Button, Form, FormControl, Container } from 'react-bootstrap';
import axios from 'axios';

import BookCards from '../BookCards/BookCards.js'

export default class Books extends React.Component{
  constructor(props){
    super(props);
    this.state={ response : [], isDeleted: false};
    this.isDeleted = this.isDeleted.bind(this);
  }

  componentWillMount(){
    var url = 'http://localhost:8095/v1/getAllBooks';
    axios.get(url).then(response => {
      console.log(response);
      this.setState({response: response.data})
    })
  }

  isDeleted(){
    console.log("called back");
    this.setState({isDeleted: true})
  }

  render(){
    if(this.state.response!=0){
      var bookCardsString = this.state.response.map((result)=>{
        var book = {
          id: result._id,
          title: result.title,
          author: result.authors,
          description: result.description,
          price: result.price,
          quantity: result.quantity
        };
        return (

              <Col>
                <BookCards key={result.title} data={book} deleted={this.isDeleted}/>
              </Col>)
      })
    }
    return(
      <div>
        <div>
        <Row>
          <Col><h1> Books Inventory </h1></Col>
          <Col></Col>
          <Col><Button variant="primary">Add Book</Button></Col>
          <Col>
            <Form inline>
              <FormControl type="text" placeholder="Search" className="mr-sm-2" />
              <Button variant="outline-success">Search</Button>
            </Form>
          </Col>
        </Row>
        </div>
        <div>
        <Container>
          <Row>
              {bookCardsString}
          </Row>
        </Container>

        </div>
      </div>
    )
  }
}
