import React, { Component } from 'react';
import { Card, Button, Col, Row, Container, Modal, Img } from 'react-bootstrap';
import axios from 'axios';

export default class BookCards extends React.Component{
  constructor(props){
    super(props);
    this.state={deleted: false, open: false, show:false}
    this.delete = this.delete.bind(this);
    this.handleShow = this.handleShow.bind(this);
    this.handleClose = this.handleClose.bind(this);
    this.saveChanges = this.saveChanges.bind(this);
  }

  componentWillMount(){
    console.log("this is props data",this.props.data);
  }

  delete(e){
    console.log("ref id is : ", this.id.textContent); //getting the value of paragraphs textContent
    var id = this.id.textContent;
    var url = `http://localhost:8095/v1/deleteBook/${id}`;
    axios.delete(url).then(response => {
      // this.setState({deleted: true})
          this.props.deleted();
    })
  }

  saveChanges(){
    console.log("here");
    console.log(this.image.src);
    console.log(this.title.textContent);
    console.log(this.author.textContent);
    console.log(this.price.value);
    console.log(this.quantity.value);
    console.log(this.props.data.description);
    console.log(this.props.data.id);

    var book = {
      "_id":this.props.data.id,
      "title":this.title.textContent,
      "authors":this.author.textContent,
      "price":this.price.value,
      "quantity":this.quantity.value,
      "description":this.props.data.description,
      "image":this.image.src
    }

    var id = this.props.data.id;
    console.log("id is :",id);
    console.log("book: ", book);
    var url = `http://localhost:8095/v1/editBook/${id}`;
    axios.put(url, book).then(response => {
      console.log(response);
    })
  }


  handleClose() {
    this.setState({ show: false });
  }

  handleShow() {
    console.log("here");
    this.setState({ show: true });
  }

  render(){
    if(this.state.show == true){
      console.log("here2");
      var displayModalPopup = (

        <Modal
        size="lg"
        aria-labelledby="example-modal-sizes-title-lg"
        show={this.state.show} onHide={this.handleClose}>
          <Modal.Header closeButton>
            <Modal.Title>Edit Book</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <Container>
              <Row>
                <Col>
                  <img variant="top"
                        src="https://purepng.com/public/uploads/large/purepng.com-booksbookillustratedwrittenprintedliteratureclipart-1421526451707uyace.png"
                        height="200" width="200"
                        ref={node => this.image = node}/>
                </Col>
                <Col>
                  <Row><h2 ref={node => this.title = node}> {this.props.data.title} </h2></Row>
                  <Row><h3> by  </h3> <h3 ref={node => this.author = node}> {this.props.data.author}</h3> </Row>
                  <Row><input type="text" placeholder="Price" size="50"
                        ref={node => this.price = node}/> </Row>
                  <br/>
                  <Row><input type="text" placeholder="Avilable Quantity" size="50"
                        ref={node => this.quantity = node}/> </Row>
                </Col>
              </Row>
              <Row>
                <h5> Description:  </h5>
                {this.props.data.description}
              </Row>
            </Container>
          </Modal.Body>
          <Modal.Footer>
            <Button variant="secondary" onClick={this.handleClose}>
              Close
            </Button>
            <Button variant="primary" onClick={this.saveChanges}>
              Save Changes
            </Button>
          </Modal.Footer>
        </Modal>

      )
    }

    return(
      <div>
        <div>
          <Card style={{ width: '18rem' }}>
          <Card.Img variant="top" src="https://purepng.com/public/uploads/large/purepng.com-booksbookillustratedwrittenprintedliteratureclipart-1421526451707uyace.png" height="200" width="5"/>
          <Card.Body>
            <Card.Text ref={node => this.id = node}>{this.props.data.id}</Card.Text>
            <Card.Title>{this.props.data.title}</Card.Title>
            <Card.Text>
              Description: {this.props.data.description}
            </Card.Text>
            <Card.Text>
              Author: {this.props.data.author}
            </Card.Text>
            <Card.Text>
              Price: {this.props.data.price}
            </Card.Text>
            <Card.Text>
              Price: {this.props.data.quantity}
            </Card.Text>
            <Container>
              <Row>
                <Col>
                  <Button variant="primary" onClick={this.delete} >Delete</Button>
                </Col>
                <Col>
                  <Button variant="primary" onClick={this.handleShow}>Edit</Button>
                </Col>
              </Row>
            </Container>
          </Card.Body>
          </Card>
        </div>
        <div>
          {displayModalPopup}
        </div>
      </div>
    )
  }
}
